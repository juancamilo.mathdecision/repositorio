__author__ = 'Math'
# -*- coding: utf-8 -*-

import pandas as pd
import os as o

rootDir = '.'
print '\\'
Lista_archivos = []
Lista_archivosdf_arch = []

list_depu_nom_apel_sex = []
list_depu_nom_apel = []
list_depu_nom_sex = []

Lista_archivos_mal = []
Lista_dir_archivos_mal = []

for dirName, subdirList, fileList in o.walk(rootDir):
    # print('Directorio encontrado: %s' % dirName)
    for fname in fileList:
        # print fname, type(fname), 'xls' in fname
        if 'xls' in fname:
            Lista_archivos.append(dirName + '\\' + fname)
            # print('\t%s' % fname)


# print Lista_archivos

local = '.\\Bases_datos\\Cesantias ley anterior.xlsx'

pd.read_excel(local, 0)

# print Lista_archivos[0]
# print pd.read_excel(Lista_archivos[0],0)

for arch in Lista_archivos:
    Lista_archivosdf_arch.append(pd.read_excel(arch, 0))


def buscar_colum_nom(lis_col):
    columna = None

    for col in lis_col:
        if isinstance(col, basestring):

            # print col, 'nombre' in col.lower()
            if 'nombre' in col.lower():
                columna = col
                break

    return columna


def buscar_colum_apel(lis_col):
    columna = None

    for col in lis_col:
        if isinstance(col, basestring):

            # print col, 'nombre' in col.lower()
            if 'apellido' in col.lower():
                columna = col
                break

    return columna


def buscar_colum_sex(lis_col):
    columna = None

    for col in lis_col:
        if isinstance(col, basestring):

            # print col, 'sexo' in col.lower()
            if 'sexo' in col.lower():
                columna = col
                break

    return columna


Lista_archivosdf_arch.pop(0)
# print Lista_archivosdf_arch[1]
print Lista_archivosdf_arch[1].columns
print buscar_colum_nom(Lista_archivosdf_arch[1].columns)
print buscar_colum_sex(Lista_archivosdf_arch[2].columns)
# print Lista_archivosdf_arch[1].loc[:,[buscar_colum_nom(Lista_archivosdf_arch[1]
# .columns),buscar_colum_sex(Lista_archivosdf_arch[1].columns)]]

for i, df in enumerate(Lista_archivosdf_arch):

    nom = buscar_colum_nom(df.columns)
    sex = buscar_colum_sex(df.columns)
    apel = buscar_colum_apel(df.columns)

    v_nom = isinstance(nom, basestring)
    v_sex = isinstance(sex, basestring)
    v_apel = isinstance(apel, basestring)

    if v_nom and v_sex and v_apel:
        list_depu_nom_apel_sex.append(df.loc[:, [nom, apel, sex]])
    elif v_nom and v_apel:
        list_depu_nom_apel.append(df.loc[:, [nom, apel]])
    elif v_nom and v_sex:
        list_depu_nom_sex.append(df.loc[:, [nom, sex]])
    else:
        Lista_archivos_mal.append(df)
        Lista_dir_archivos_mal.append(Lista_archivos[i])

for df in list_depu_nom_apel_sex:
    df.columns = ['Nombres', 'Apellidos', 'Sexo']

for df in list_depu_nom_apel:
    df.columns = ['Nombres', 'Apellidos']

for df in list_depu_nom_sex:
    df.columns = ['Nombres', 'Sexo']

Dataframe_final = list_depu_nom_apel_sex[0]
list_depu_nom_apel_sex.pop(0)

for df in list_depu_nom_apel_sex:
    Dataframe_final = Dataframe_final.append(df, ignore_index=True)

for df in list_depu_nom_apel:
    Dataframe_final = Dataframe_final.append(df, ignore_index=True)

for df in list_depu_nom_sex:
    Dataframe_final = Dataframe_final.append(df, ignore_index=True)


# print Dataframe_final

Data_frame_group = Dataframe_final.drop_duplicates(
    cols=['Nombres', 'Apellidos'], take_last=True)

Data_frame_group_d = pd.DataFrame(index=[0], columns=['Nombres', 'Sexo'])

for i in range(len(Data_frame_group.index)):
    nombre = Data_frame_group.iat[i, 1]
    apellido = Data_frame_group.iat[i, 0]
    #print nombre, apellido , nombre == apellido
    if nombre == apellido:
        df_p = pd.DataFrame(
            {'Nombres': nombre, 'Sexo': Data_frame_group.iat[i, 2]}, index=[0],
            columns=['Nombres', 'Sexo'])
        #print df_p
        Data_frame_group_d = Data_frame_group_d.append(df_p, ignore_index=True)
    else:
        if (isinstance(nombre, basestring) and isinstance(apellido,
                                                          basestring)):
            nombre_complet = nombre + ' ' + apellido
            df_p = pd.DataFrame(
                {'Nombres': nombre_complet, 'Sexo': Data_frame_group.iat[i, 2]},
                index=[0], columns=['Nombres', 'Sexo'])
            Data_frame_group_d = Data_frame_group_d.append(df_p,
                                                           ignore_index=True)
        if (isinstance(nombre, basestring) and not isinstance(apellido,
                                                              basestring)):
            df_p = pd.DataFrame(
                {'Nombres': nombre, 'Sexo': Data_frame_group.iat[i, 2]},
                index=[0], columns=['Nombres', 'Sexo'])
            #print df_p
            Data_frame_group_d = Data_frame_group_d.append(df_p,
                                                           ignore_index=True)

for i in range(len(Data_frame_group_d.index)):
    sexo = Data_frame_group_d.iat[i, 1]
    if sexo == 1 and i < 25:
        Data_frame_group_d.iat[i, 1] = 'F'
    elif sexo == 1 and i > 25:
        Data_frame_group_d.iat[i, 1] = 'M'
    elif sexo == 0:
        Data_frame_group_d.iat[i, 1] = 'M'
    elif sexo == 2:
        Data_frame_group_d.iat[i, 1] = 'F'
    elif isinstance(sexo, basestring):
        if 'M' in sexo:
            Data_frame_group_d.iat[i, 1] = 'M'
        else:
            Data_frame_group_d.iat[i, 1] = 'F'

Data_frame_group_d = Data_frame_group_d.dropna(how='all')

#print Data_frame_group
#Dataframe_concat.to_csv('Depuracion2.csv', encoding = "utf-8", index=False)
Data_frame_group.to_csv('Depuracion.csv', encoding="utf-8", index=False)
Data_frame_group_d.to_csv('Depuracion2.csv', encoding="utf-8", index=False)
#Dataframe_final.to_csv('Depuracion.csv', encoding = "utf-8", index=False)
#Dataframe_final.to_excel('Depuracion.xlsx', index=False, encoding = "utf-8")

#print list_depu_nom_apel_sex[0]
#print list_depu_nom_apel[0]
#print list_depu_nom_sex[0]

#print len(lista_archivos_depu)

print '\n'
print len(Lista_archivos_mal)
print '\n'
print len(Lista_dir_archivos_mal)
print '\n'

print Lista_dir_archivos_mal
__author__ = 'Math'
import os as os
import random as r
import numpy as np


def listar_ruta_archivos_dataframe(rootdir):
    """
    Lista las rutas de los archivos a convertir en DataFrame
    :param rootDir: Directorio principal en donde buscar
    :return:
    """
    lista_archivos = []

    for dirName, subdirList, fileList in os.walk(rootdir):

        for fname in fileList:
            lista_archivos.append(os.path.join(dirName, fname))

    return lista_archivos


def buscar_colum_nom(lis_col, nomb_col):
    """
    Busca la columna cuyo nombre se especifica en el paremetro nomb_col
    :param lis_col: Lista de las columnas de un DataFrame
    :param nomb_col: Nombre de la columna a buscar
    :return: Si encuentra el nombre en la lista devuelve su nombre, en caso
    contrario retorna None
    """

    columna = None

    for col in lis_col:
        if isinstance(col, basestring):

            # print col, 'nombre' in col.lower()
            if nomb_col in col.lower():
                columna = col
                break

    return columna


def rellenar_col_sex(prob):
    sex = None
    if r.random() < prob:
        sex = 'F'
    else:
        sex = 'M'

    return sex


def eliminar_fila_df(df, indx):
    df.iloc[:1] = np.nan
    df.dropna(how='all')
    return df


def fila_tolist(fila):
    list = []
    for i in range(len(fila.columns)):
        list.append(fila.iat[0, i])

    return list


def depuracio(lista_archivos_dataframe, lista_archivos_mal, lista_dir_archivos_mal,
           lista_archivos, sw):

    list = []
    list_depu_nom_apel_sex = []
    list_depu_nom_apel = []
    list_depu_nom_sex = []

    for i, df in enumerate(lista_archivos_dataframe):
        nom = buscar_colum_nom(df.columns, 'nombre')
        sex = buscar_colum_nom(df.columns, 'sexo')
        apel = buscar_colum_nom(df.columns, 'apellido')

        v_nom = isinstance(nom, basestring)
        v_sex = isinstance(sex, basestring)
        v_apel = isinstance(apel, basestring)

        if v_nom and v_sex and v_apel:
            list_depu_nom_apel_sex.append(df.loc[:, [nom, apel, sex]])
        elif v_nom and v_apel:
            list_depu_nom_apel.append(df.loc[:, [nom, apel]])
        elif v_nom and v_sex:
            list_depu_nom_sex.append(df.loc[:, [nom, sex]])
        else:
            if sw == 1:
                lista_archivos_mal.append(df)
                lista_dir_archivos_mal.append(lista_archivos[i])
            else:
                lista_dir_archivos_mal.pop[i]

    list.append(list_depu_nom_apel_sex)
    list.append(list_depu_nom_apel)
    list.append(list_depu_nom_sex)
    list.append(lista_archivos_mal)
    list.append(lista_dir_archivos_mal)
    return list





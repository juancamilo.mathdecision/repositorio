__author__ = 'Math'
# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np
from functions import funciones as f

lista_archivos_dataframe = []
# Lista de todos los DataFrames importados

list_depu_nom_apel_sex = []
# Lista de DataFrames que contienen las columnas Nombres, Apellidos y Sexo
list_depu_nom_apel = []
# Lista DataFrames que contienen solo las columnas Nombres y Apellidos
list_depu_nom_sex = []
# Lista DataFrames que contienen solo las columnas Nombres y Sexo 

lista_archivos_mal = []
# Lista de DataFrames que no contienen las columnas Nombres,
#Apellidos y Sexo cuando se importaron
lista_dir_archivos_mal = []
#Lista de las rutas de los DataFrames de la lista anterior

lista_archivos = f.listar_ruta_archivos_dataframe('.\\Bases_datos')
#lista de las rutas de todos los archivos de la carpeta Bases_datos

for arch in lista_archivos:
    lista_archivos_dataframe.append(pd.read_excel(arch, 0))
#Lista de Todos los DataFrame importados

#El siguente Funcion la lista que contiene todos los DataFrames separando
#los DataFrames que contienen las columnas nombres, apellidos, sexo de los que
#solo contiene solo dos de estos atributos

list = f.depuracio(lista_archivos_dataframe, lista_archivos_mal,
                lista_dir_archivos_mal,
                lista_archivos, 1)

list_depu_nom_apel_sex = list[0]
list_depu_nom_apel = list[1]
list_depu_nom_sex = list[2]
lista_archivos_mal = list[3]
lista_dir_archivos_mal = list[4]

#Los siguiente for renombran las columnas de los DataFrames Depurados
for df in list_depu_nom_apel_sex:
    df.columns = ['Nombres', 'Apellidos', 'Sexo']

for df in list_depu_nom_apel:
    df.columns = ['Nombres', 'Apellidos']

for df in list_depu_nom_sex:
    df.columns = ['Nombres', 'Sexo']

dataframe_final = list_depu_nom_apel_sex[0]
#DataFrame final que va contener las columnas Nombres, Apellidos, Sexo con toda
# la informacion depurada

list_depu_nom_apel_sex.pop(0)

#En los siguientes for va acumulando todos los DataFrames Depurados en un solo
#DataFrame, en Dataframe_final
for df in list_depu_nom_apel_sex:
    dataframe_final = dataframe_final.append(df, ignore_index=True)

for df in list_depu_nom_apel:
    dataframe_final = dataframe_final.append(df, ignore_index=True)

for df in list_depu_nom_sex:
    dataframe_final = dataframe_final.append(df, ignore_index=True)

data_frame_group = dataframe_final.drop_duplicates(
    cols=['Nombres', 'Apellidos'], take_last=True)
#Eliminado los datos rePetidos en las columnas Nombres y Apellidos

data_frame_group_d = pd.DataFrame(index=[0], columns=['Nombres', 'Sexo'])
#Dataframe que Va a contener la depuracion y la concatenacion de las columnas
#Nombres y Apellidos del datos en el DataFrame Data_frame_group

#El siguente for concatena las Columnas Nombres y Apellidos del DataFrame
#Data_frame_group
for i in range(len(data_frame_group.index)):
    nombre = data_frame_group.iat[i, 1]
    apellido = data_frame_group.iat[i, 0]
    # print nombre, apellido , nombre == apellido
    if nombre == apellido:
        df_p = pd.DataFrame(
            {'Nombres': nombre, 'Sexo': data_frame_group.iat[i, 2]}, index=[0],
            columns=['Nombres', 'Sexo'])
        #print df_p
        data_frame_group_d = data_frame_group_d.append(df_p, ignore_index=True)
    else:
        if (isinstance(nombre, basestring) and isinstance(apellido,
                                                          basestring)):
            nombre_complet = nombre + ' ' + apellido
            df_p = pd.DataFrame(
                {'Nombres': nombre_complet, 'Sexo': data_frame_group.iat[i, 2]},
                index=[0], columns=['Nombres', 'Sexo'])
            data_frame_group_d = data_frame_group_d.append(df_p,
                                                           ignore_index=True)
        if (isinstance(nombre, basestring) and not isinstance(apellido,
                                                              basestring)):
            df_p = pd.DataFrame(
                {'Nombres': nombre, 'Sexo': data_frame_group.iat[i, 2]},
                index=[0], columns=['Nombres', 'Sexo'])
            #print df_p
            data_frame_group_d = data_frame_group_d.append(df_p,
                                                           ignore_index=True)

#El siguente for Cambia todos los valores del sexo a su F: Femenino
#M: Masculino
for i in range(len(data_frame_group_d.index)):
    sexo = data_frame_group_d.iat[i, 1]
    if sexo == 1 and i < 25:
        data_frame_group_d.iat[i, 1] = 'F'
    elif sexo == 1 and i > 25:
        data_frame_group_d.iat[i, 1] = 'M'
    elif sexo == 0:
        data_frame_group_d.iat[i, 1] = 'M'
    elif sexo == 2:
        data_frame_group_d.iat[i, 1] = 'F'
    elif isinstance(sexo, basestring):
        if 'M' in sexo:
            data_frame_group_d.iat[i, 1] = 'M'
        else:
            data_frame_group_d.iat[i, 1] = 'F'

data_frame_group_d = data_frame_group_d.dropna(how='all')

df_nan = data_frame_group_d.copy()

print data_frame_group_d.groupby('Sexo').count(), len(data_frame_group_d)

for i in range(len(data_frame_group_d)):
    sex = data_frame_group_d.iat[i, 1]
    if not isinstance(sex, basestring):
        df_nan.iat[i, 1] = f.rellenar_col_sex(714.0 / (714 + 320))

data_frame_group_d.to_csv('Depuracion.csv', encoding="utf-8", index=False)
df_nan.to_csv('Depuracion_2.csv', encoding="utf-8", index=False)

print lista_dir_archivos_mal

print len(lista_dir_archivos_mal)
print '\ng'